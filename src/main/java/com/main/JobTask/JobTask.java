package com.main.JobTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobTask {

	public static void main(String[] args) {
		SpringApplication.run(JobTask.class, args);
	}

}
