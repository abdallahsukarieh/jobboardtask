package com.main.JobTask.service;

import java.util.List;

import com.main.JobTask.model.User;
import com.main.JobTask.model.UserDto;

public interface UserService {
    User save(UserDto user);
    List<User> findAll();
    User findOne(String username);
}
