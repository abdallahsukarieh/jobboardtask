package com.main.JobTask.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.main.JobTask.model.JobOffers;

@Service
public class JobOffersServices{

	@Autowired
	private JobOffersRepo joboffersrepo;
	public boolean status;
	
	public Iterable<JobOffers> findAllOffers(){
		
		return joboffersrepo.findAll();
	}

	public JobOffers saveOffer(JobOffers joboffers) {
		return joboffersrepo.save(joboffers);
	}
	
	public Optional<JobOffers> findById(Long id) {
		return joboffersrepo.findById(id);
	}


	
	 @Transactional
	    public void update(long id, JobOffers source) {
		 if(source.getStatus()==true) {source.setStatus(status);}
		 
		 joboffersrepo.save(source);
		 
	 };
	    } 

	

