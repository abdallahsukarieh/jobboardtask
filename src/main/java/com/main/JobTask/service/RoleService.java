package com.main.JobTask.service;

import com.main.JobTask.model.Role;

public interface RoleService {
    Role findByName(String name);
}
