package com.main.JobTask.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.main.JobTask.model.JobOffers;

@Repository
public interface JobOffersRepo extends CrudRepository<JobOffers, Long> {

}
