package com.main.JobTask.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import com.main.JobTask.config.TokenProvider;
import com.main.JobTask.model.AuthToken;
import com.main.JobTask.model.JobOffers;
import com.main.JobTask.model.LoginUser;
import com.main.JobTask.model.User;
import com.main.JobTask.model.UserDto;
import com.main.JobTask.service.JobOffersServices;
import com.main.JobTask.service.UserService;

import java.util.List;
import java.util.Optional;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Autowired
    private UserService userService;
    
    @Autowired
    private JobOffersServices jobservices;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> generateToken(@RequestBody LoginUser loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        return ResponseEntity.ok(new AuthToken(token));
    }

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public User saveUser(@RequestBody UserDto user){
        return userService.save(user);
    }



    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/adminping", method = RequestMethod.GET)
    public List<User> findAllUser(@RequestBody UserDto user){
        return userService.findAll();
    }
    
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public Iterable<JobOffers> findAllOffers(){
        return jobservices.findAllOffers();
    }
    
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/getOfferById{id}", method = RequestMethod.GET)
    public Optional<JobOffers> getOfferById(@PathVariable Long id) {
    	
    	return jobservices.findById(id);
    }
    
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/Create", method = RequestMethod.POST)
    public JobOffers createNewOffer(@RequestBody JobOffers joboffer) {
    	return jobservices.saveOffer(joboffer);
    }
    
    
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/Update", method = RequestMethod.POST)
    @PutMapping("updateEmployee/{id}")
    public void updateEmployee(@PathVariable long id, @RequestBody JobOffers joboffer) {
    	jobservices.update(id, joboffer);
    }
    
    	
    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value="/userping", method = RequestMethod.GET)
    public String userPing(){
        return "Any User Can Read This";
    }

}
