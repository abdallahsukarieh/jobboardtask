package com.main.JobTask.model;

import javax.persistence.*;

@Entity
@Table(name = "offers")
public class JobOffers {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	  @Column
	    private String title;

	  @Column
	    private String desc;
	  
	  @Column
	    private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public JobOffers(Long id, String title, String desc, boolean status) {
		super();
		this.id = id;
		this.title = title;
		this.desc = desc;
		this.status = status;
	}

	public JobOffers() {
		super();
		// TODO Auto-generated constructor stub
	}
	  
	  
}
